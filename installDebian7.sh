#!/bin/sh
#this approach needs superuser privileges, and is thought for being used with
#a new install of a digitaloceans Debian7.0 droplet

git config --global user.name "Imobach Martin"
git config --global user.email "kabomi@gmail.com"

#node
wget http://nodejs.org/dist/v0.12.7/node-v0.12.7-linux-x64.tar.gz
wget http://nodejs.org/dist/npm/npm-2.11.3.tgz
tar -zvxf node-v0.12.7-linux-x64.tar.gz
mv node-v0.12.7-linux-x64 /opt
tar -zvxf npm-2.11.3.tgz
mv npm /opt
echo "PATH=$PATH:/opt/node-v0.12.7-linux-x64/bin" | tee -a .bashrc

#mongodb
wget http://downloads.mongodb.org/linux/mongodb-linux-x86_64-2.4.8.tgz
tar -zxvf mongodb-linux-x86_64-2.4.8.tgz
mv mongodb-linux-x86_64-2.4.8 /opt/mongodb
mkdir -p /data/db
echo "PATH=$PATH:/opt/mongodb/bin" | tee -a .bashrc
mongod &

#fontconfig needed by phantomjs
aptitude install fontconfig
mkdir /var/www
cd /var/www
git clone https://kabomi@bitbucket.org/kabomi/berlin.git
cd berlin
npm install
npm install grunt-cli@0.1.11 -g

export BERLIN_DEPLOY=true

grunt deploy