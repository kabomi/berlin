// Karma configuration
// Generated on Tue Dec 10 2013 22:22:54 GMT+0000 (WET)

module.exports = function(config) {
    var pkg = require('../package');
    var CLIENT_DIR = pkg.clientDirs.client;
    var CLIENT_SPEC_DIR = pkg.clientDirs.specs;
    var BOWER_DIR = pkg.clientDirs.vendor || 'bower_components/';

  config.set({

    // base path, that will be used to resolve files and exclude
    basePath: '..',


    // frameworks to use
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
        BOWER_DIR + 'jquery/dist/jquery.js',
        //CLIENT_DIR + 'assets/javascript/jquery-*.js',
        CLIENT_DIR + 'assets/javascript/signals.js',
        CLIENT_DIR + 'assets/javascript/crossroads.js',
        {pattern: CLIENT_SPEC_DIR + 'karma/htmlFixture.html', included: true},
        {pattern: CLIENT_SPEC_DIR + 'karma/loadFixtures.js', included: true},
        CLIENT_DIR + 'debug.js',
        CLIENT_DIR + 'Common.js',
        /*your files here */
        CLIENT_DIR + 'repository.js',
        CLIENT_DIR + 'utils.js',
        CLIENT_DIR + 'factory.js',
        //CLIENT_DIR + 'bundle.js',
        CLIENT_DIR + 'app.js',
        CLIENT_SPEC_DIR + '_lib_spec.js',
        CLIENT_SPEC_DIR + '*spec.js'
    ],


    // list of files to exclude
    exclude: [

    ],



    // test results reporter to use
    // possible values: 'dots', 'progress', 'junit', 'growl', 'coverage', 'osx'
    reporters: (process.env.BERLIN_DEPLOY?['progress']:['dots', 'growl', 'osx']),


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: (process.env.BERLIN_DEPLOY?false:true),


    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera (has to be installed with `npm install karma-opera-launcher`)
    // - Safari (only Mac; has to be installed with `npm install karma-safari-launcher`)
    // - PhantomJS
    // - IE (only Windows; has to be installed with `npm install karma-ie-launcher`)
    browsers: (process.env.BERLIN_DEPLOY?['PhantomJS']:['PhantomJS']),


    // If browser does not capture in given timeout [ms], kill it
    captureTimeout: 60000,


    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: (process.env.BERLIN_DEPLOY?true:false)
  });
};
