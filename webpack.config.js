module.exports = {
    //entry: ["./src/client/app.coffee"],
    context: __dirname + "/src/client/es6",
    entry: ["./app.es6"],
    output: {
        path: __dirname + "/src/client",
        filename: "bundle.js"
    },
    module: {
        loaders: [
            { test: /\.coffee$/, exclude: /node_modules/, loader: "coffee-loader" },
            { test: /\.(coffee\.md|litcoffee)$/, exclude: /node_modules/, loader: "coffee-loader?literate" },
            { test: /\.es6$/, exclude: /node_modules/, loader: "babel"}
        ]
    }
};
