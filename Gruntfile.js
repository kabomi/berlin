"use strict";

module.exports = function(grunt) {
    var pkg = require('./package');
    var NODE_VERSION = "v" + pkg.engines.node + "\n";
    var BUILD_DIR = pkg.tempDirs.build || "dist/build";
    var SRC_DIR = pkg.tempDirs.src || "src";
    var BUILD_SRC_DIR = BUILD_DIR + '/' + SRC_DIR;
    var GENERATED_DIR = pkg.tempDirs.root;
    var TEMP_TESTFILE_DIR = GENERATED_DIR + pkg.tempDirs.test;
    var SERVER_RUNNER = pkg.serverDirs.runner;
    var SERVER_RUNNER_PORT = process.env.BERLIN_PORT || "8080";
    var SERVER_SPEC_DIR = pkg.serverDirs.spec;
    var SERVER_DEPLOY_ALLOWED = process.env.BERLIN_DEPLOY || false;
    var MONGODB_PORT = process.env.MONGODB_PORT || "27017";

    var common = require('./build/build_options.js');
    var fs = require('fs');

    var uglify_targets = [];
    var uglify_target_client = {};
    var uglify_target_server = {
        expand: true,
        cwd: BUILD_SRC_DIR + '/server/',
        src: ['*.js', '!*.min.js', '../lib/*.js'],
        dest: BUILD_SRC_DIR + '/server/'
    };
    uglify_target_client[BUILD_SRC_DIR + '/client/app.min.js'] = [
        BUILD_SRC_DIR + '/client/utils.js',
        BUILD_SRC_DIR + '/client/*.js'
    ];
    uglify_targets.push(uglify_target_client);
    uglify_targets.push(uglify_target_server);
    grunt.initConfig({

        pkg: require('./package'),

        jshint: {
            node: {
                files: {
                    src: ['src/server/**/*.js', 'build/**/*s.js', 'src/_*.js']
                },
                options: common.nodeLintOptions()
            },
            browser: {
                files: {
                    src: ['src/client/*.js', 'src/client/spec/*.js', 'src/casper*.js']
                },
                options: common.browserLintOptions()
            }
        },

        jasmine_node: {
            growl: true,
            options: common.nodeJasmineOptions()
        },

        sass: {
            dist: {
                files: [{
                    expand: true,
                    src: ['src/client/**/*.sass'],
                    ext: '.css'
                }]
            }
        },

        watch: {
            server: {
                files: [ 'src/server/**/*.js'],
                tasks: [ 'testServer' ]
            },
            sass:{
                files: [ 'src/client/**/*.sass'],
                tasks: [ 'sass' ],
                options: {
                    livereload: true,
                }
            },
            lint:{
                files: [ 'Gruntfile.js', 'src/client/**/*.js', 'src/server/**/*.js'],
                tasks: [ 'jshint' ],
                options: {
                  livereload: true,
                }
            }
        },

        exec: {
            node: {
                cmd: function() {
                    var command = 'node --version';
                    console.log("> " + command);
                    return command;
                },
                callback: function(error, stdout, stderr){
                    if (stdout !== NODE_VERSION){
                        grunt.fatal("Incorrect node version. Expected " + NODE_VERSION);
                    }
                }
            },
            kill_node_servers: {
                cmd: function() {
                    var command = 'killall -9 -q -e node';
                    console.log("Stopping all node servers");
                    console.log("> " + command);
                    return command;
                },
                callback: function(error, stdout, stderr){
                    if (error || stderr){
                        console.log("Something went wrong, but will try to move on.");
                        //grunt.fatal("Fix what went wrong and try again.");
                    }else{
                        console.log("[OK]Servers stopped successfully");
                        //grunt.task.run(['test', 'cmd:start_server']);
                    }
                }
            },
            git_pull_origin: {
                cmd: function() {
                    var command = 'git pull origin master';
                    console.log("Pull from origin master");
                    console.log("> " + command);
                    return command;
                },
                callback: function(error, stdout, stderr){
                    if (error){
                        grunt.fatal("Fix what went wrong and try again." + error);
                    }else{
                        console.log("[OK]Git up to date");
                    }
                }
            },
            remove_unnecessary_tests: {
                cmd: function() {
                    var command = 'rm -f ' + SERVER_SPEC_DIR +'/' + 'heroku* ' + SERVER_SPEC_DIR +'/' + 'digitalocean*';
                    console.log("Remove unnecessary tests for production");
                    console.log("> " + command);
                    return command;
                },
                callback: function(error, stdout, stderr){
                    if (error){
                        console.log("Something went wrong, but will try to move on.");
                        //grunt.fatal("Fix what went wrong and try again.");
                    }else{
                        console.log("[OK]Unnecessary tests removed");
                    }
                }
            },
            start_mongodb: {
                cmd: function() {
                    var command = 'mongod --port ' + MONGODB_PORT;
                    console.log("> " + command);
                    return command;
                },
                exitCodes: [0, 100],
                callback: function(error, stdout, stderr){
                    if (error || stderr){
                        if(stdout.indexOf('a mongod instance already running') < 0)
                            grunt.fatal("Couldn't start mongod demon.");
                        else{
                            console.log("[OK]Mongodb was already started");
                        }
                    }else{
                        console.log("[OK]Mongodb started successfully");
                    }
                }
            },
            start_server: {
                cmd: function() {
                    var command = 'node ' + SERVER_RUNNER + ' ' + SERVER_RUNNER_PORT;
                    console.log("> " + command);
                    return command;
                },
                callback: function(error, stdout, stderr){
                    if (error || stderr){
                        grunt.fatal("Couldn't start the server.");
                    }else{
                        console.log("[OK]Server started successfully");
                    }
                }
            },
            testClient_task: {
                cmd: function() {
                    var command = 'node ' + 'node_modules/.bin/karma' + ' run build/karma.conf.js' ;
                    console.log("> " + command);
                    return command;
                },
                callback: function(error, stdout, stderr){
                    if (error && stderr){
                        grunt.fatal("Test runner maybe not started jet.");
                    }else{
                        if(error){
                            grunt.fatal("Test runner dropped errors.");
                        }
                        console.log("[OK]Client tests were run successfully");
                    }
                }
            },
            testRunner_task: {
                cmd: function() {
                    var command = 'node ' + 'node_modules/.bin/karma' + ' start --single-run --no-auto-watch build/karma.conf.js';
                    console.log("> " + command);
                    return command;
                },
                callback: function(error, stdout, stderr){
                    if (error && stderr){
                        grunt.fatal("Test runner aborted.");
                    }else{
                        if(error){
                            grunt.fatal("Test runner dropped errors.");
                        }
                        console.log("[OK]Test Runner running");
                    }
                }
            }
        },

        clean: {
            build: [BUILD_DIR + '/*'],
            spec: [BUILD_DIR + '/**/spec', BUILD_DIR + '/**/*_spec.js', BUILD_DIR + '/**/spec_*.js', BUILD_DIR + '/**/debug.js'],
            client_js: [BUILD_SRC_DIR + '/client/*.js', '!' + BUILD_SRC_DIR + '/client/*.min.js']
        },

        copy: {
            main: {
                files: [
                    // includes files within path and its sub-directories
                    {expand: true, src: [SRC_DIR + '/**'], dest: BUILD_DIR }
                ]
            },
            content: {
                files: [{
                    expand: true,
                    cwd: BUILD_SRC_DIR + '/',
                    src: ['**/*.html'],
                    dest: BUILD_SRC_DIR + '/'
                }],
                options: {
                    process: function (content, srcpath) {
                        var newContent = "";
                        console.log("path: "+ srcpath);
                        newContent = content.replace(/<script.*src="\/client\/.*<\/script>/g,"");
                        newContent = newContent.replace(/\s*<\/head>/, "\n\t<script type='text/javascript' src='/client/app.min.js'></script>\n</head>");
                        
                        return newContent;
                    }
                }
            },
            win32: {
                files: [
                    {expand: true, cwd: 'dist/static/win32' + '/', src: ['**/*'], dest: BUILD_DIR + '/win32'},
                    {expand: true, cwd: BUILD_SRC_DIR + '/', src: ['**/*'], dest: BUILD_DIR + '/win32/' + SRC_DIR}
                ],
            }
        },

        uglify: {
            options: {
                mangle: true,
                compress: {
                    drop_console: true
                }
            },
            my_target: {
                files: uglify_targets
            }
        },

        compress: {
            win32_build: {
                options: {
                    archive: BUILD_DIR + '/' + pkg.name + '-win32-build-' + (parseInt(pkg.build ) + 1) + '.zip'
                },
                files: [
                    {expand: true, cwd: BUILD_DIR + '/' + pkg.name + '/', src: ['**/*'], dest: pkg.name + '/'} // makes all src relative to cwd
                ]
            }
        }
        
    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-bump');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-exec');

    grunt.registerTask('testServer_task', function(){
        testNodeJasmineWith.call(this, common.nodeJasmineOptions());
    });

    grunt.registerTask('smokeTests_task', function(){
        testNodeJasmineWith.call(this, common.smokeJasmineOptions());
    });

    function testNodeJasmineWith(options){
        var jasmine = require("jasmine-node");

        var done = this.async();
        options.onComplete = function(runner, log){
            if (runner.results().failedCount !== 0) {
                if(options.growl){
                    console.log("Jasmine node tests failed. Continue so that growl reporter works.");
                }else{
                    grunt.warn("Jasmine node tests failed.");
                }
            }else{
                grunt.task.run('clean');
            }
            done();
        };
        try {
            // since jasmine-node@1.0.28 an options object need to be passed
            jasmine.executeSpecsInFolder(options);
        } catch (e) {
            console.log('Failed to execute "jasmine.executeSpecsInFolder": ' + e.stack);
            grunt.fatal("Jasmine node tests failed.");
        }
    }
    grunt.registerTask('testClient_task', ['exec:testClient_task']);

    grunt.registerTask('testServer', 'Test only the server', [ 'exec:node', 'jshint:node', 'temp_testfile_directory', 'testServer_task']);
    grunt.registerTask('testClient', 'Test only the client', [ 'exec:node', 'jshint:browser', 'temp_testfile_directory', 'testClient_task', 'clean_test_files']);
    grunt.registerTask('smoke', 'Test only smoke tests', [ 'exec:node', 'jshint', 'temp_testfile_directory', 'smokeTests_task', 'clean_test_files']);
    grunt.registerTask('test', 'Test unit tests', [ 'testServer', 'testClient']);
    grunt.registerTask('default',   [ 'test' ]);
    grunt.registerTask('integrate', 'Instructions about how to integrate code in production when in a group', [ 'test', 'integrate_commands' ]);
    grunt.registerTask('deploy', '[BEWARE: to be run on production only] Rerun the server with the latest commit in origin:master',
            ['deployment_allowed', 'exec:start_mongodb', 'exec:kill_node_servers', 'exec:git_pull_origin',
             'exec:remove_unnecessary_tests', 'testServer', 'exec:testRunner_task', 'clean_test_files', 'exec:start_server']);

    grunt.registerTask('deployment_allowed', function(){
        if (!SERVER_DEPLOY_ALLOWED){
            grunt.fatal("Deploy command not allowed in this environment");
        }
    });
    grunt.registerTask('integrate_commands', function(){
        var commands = require('./build/integration_commands');
        commands.print();
    });

    grunt.registerTask('temp_testfile_directory', function(){
        console.log("TESTFILE_DIR: " +  TEMP_TESTFILE_DIR);
        if (!fs.existsSync(GENERATED_DIR)){
            fs.mkdirSync(GENERATED_DIR);
        }
        if (!fs.existsSync(TEMP_TESTFILE_DIR)){
            fs.mkdirSync(TEMP_TESTFILE_DIR);
        }
    });

    grunt.registerTask('clean_test_files', function(){
        console.log("REMOVE TESTFILE_DIR: " +  TEMP_TESTFILE_DIR);
        if (fs.existsSync(TEMP_TESTFILE_DIR)){
            fs.rmdirSync(TEMP_TESTFILE_DIR);
        }
        if (fs.existsSync(GENERATED_DIR)){
            fs.rmdirSync(GENERATED_DIR);
        }
    });

    grunt.registerTask('rename_win32', function(){
        var buildwin32dir = BUILD_DIR + '/win32';
        var buildwin32newdir = BUILD_DIR + '/' + pkg.name;
        console.log("RENAME WIN32 DIR TO: " +  buildwin32newdir);
        fs.rename(buildwin32dir, buildwin32newdir, function (err) {
            if (err) throw err;
            grunt.log.ok('renamed complete');
        });
    });

    grunt.registerTask('zip_win32', function(){
        var inputFolderName = BUILD_DIR + '/' + pkg.name;
        var outputFileName = BUILD_DIR + '/' + pkg.name + '-win32-build-' + pkg.build + '.zip';
        console.log("ZIP " + inputFolderName + " DIR TO: " +  outputFileName);

        var archiver = require('archiver');

        var output = fs.createWriteStream(outputFileName);
        var zipArchive = archiver('zip');

        output.on('close', function() {
            console.log(archive.pointer() + ' total bytes');
            grunt.log.ok('Zip builded successfully');
        });

        zipArchive.pipe(output);

        zipArchive.bulk([
            { src: [ '**/*' ], cwd: inputFolderName + '/', expand: true }
        ]);

        zipArchive.finalize();
    });

    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
    grunt.registerTask('bump_build', function(){
        var VERSION_REGEXP = /([\'|\"]?build[\'|\"]?[ ]*:[ ]*[\'|\"]?)([\d||A-a|.|-]*)([\'|\"]?)/i;
        console.log("BUMPING BUILD");
        var buildNumber = pkg.build || 0;

        if (!isNumeric(buildNumber)) throw "Build value is not a number";

        var newBuildNumber = parseInt(buildNumber) + 1;

        var content = grunt.file.read('package.json').replace(VERSION_REGEXP, function(match, prefix, parsedVersion, suffix) {
            console.log(prefix + newBuildNumber + suffix);
            //gitVersion = gitVersion && parsedVersion + '-' + gitVersion;
            //version = exactVersionToSet || gitVersion || semver.inc(parsedVersion, versionType || 'patch');
            return prefix + newBuildNumber + suffix;
        });
        grunt.file.write('package.json', content);
        grunt.log.ok('Version bumped from ' + buildNumber + ' TO: ' +  newBuildNumber);

    });

    grunt.registerTask('build', 'Generate files ready for distribution', ['clean:build', 'copy:main', 'clean:spec', 'uglify', 'clean:client_js', 'copy:content']);
    grunt.registerTask('win32', 'Generate win32 release', ['build', 'copy:win32', 'rename_win32', 'bump_build', 'compress:win32_build']);

};
