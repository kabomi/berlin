"use strict";

(function(){

    var _DEBUG = !process.env.BERLIN_DEPLOY || true;
    function consoleLog(text){
        if (_DEBUG){
            console.log(text);
        }
    }
    var log = consoleLog;
    function isNumber(value){
        return typeof value === "number" && isFinite(value);
    }
    function printObj(obj, title){
        if (!_DEBUG) return;

        log("[TITLE]:" + title);
        for (var name in obj){
            if (obj.hasOwnProperty(name)){
                var prop = obj[name];
                if (typeof(prop) !== 'function'){
                    log(name + ":" + prop + " [" + typeof(prop) + "]");
                }else{
                    log(name + ": [Function]");
                }
            }
        }
    }
    function isNotArray(value){
        return !isArray(value);
    }
    function isArray(value){
        var result = (value !== undefined &&
            typeof value === 'object' &&
            typeof value.length === 'number' &&
            typeof value.splice === 'function' &&
            !(value.propertyIsEnumerable('length')));
        return result;
    }
    function indexOfArrayItemById(array, item){
        var index = -1;
        if (item.id === undefined || item.id === null) return;
        if (isNotArray(array)) return;

        array.forEach(function(ele, i){
            if (ele.id === item.id ||
                ele._id === item._id){

                index = i;
            }
        });
        return index;
    }
    function decodeUri(value){
        if (typeof(value) === 'object' &&
                value !== null){

            return value;
        }
        return decodeURIComponent(value);
    }
    function runCallbackForEachArrayItem(array, callback){
        for (var i in array){
            var arrayItem = array[i];
            callback(arrayItem);
        }
    }
    function now(){
        var date = new Date();
        
        return date.toDateString();
    }


    exports.log = consoleLog;
    exports.isNumber = isNumber;
    exports.printObj = printObj;
    exports.isNotArray = isNotArray;
    exports.isArray = isArray;
    exports.indexOfArrayItemById = indexOfArrayItemById;
    exports.decodeUri = decodeUri;
    exports.runCallbackForEachArrayItem = runCallbackForEachArrayItem;
    exports.now = now;
    
})();