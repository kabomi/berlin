/*global describe, it, expect, spyOn, beforeEach, fs, xit, done, jasmine, afterEach, xdescribe */

"use strict";

(function(){

    var pkg = require('../../../package');
    var TEMP_DIR = pkg.tempDirs.root + pkg.tempDirs.test;
    var TEST_FILE = TEMP_DIR + "/test.html";
    var TEST_FILE_404 = TEMP_DIR + "/test404.html";
    var ASSETS_DIR = TEMP_DIR + "/assets/";
    var ASSET_FILE = "/asset.js";
    var TEST_FILE_ASSETS = ASSETS_DIR + ASSET_FILE;
    var CLIENT_APP_DIR = TEMP_DIR + "/client/";
    var CLIENT_APP_FILE = "app.js";
    var CLIENT_SPEC_FILE = "test_spec.js";
    var TEST_FILE_CLIENT_APP = CLIENT_APP_DIR + CLIENT_APP_FILE;
    var TEST_FILE_CLIENT_SPEC = CLIENT_APP_DIR + CLIENT_SPEC_FILE;
    var TEST_FILE_CLIENT_SPEC_FOLDER = CLIENT_APP_DIR + 'spec/' + CLIENT_APP_FILE;
    var TEST_FILE_CLIENT_IMAGE_FOLDER = CLIENT_APP_DIR + 'images/' + CLIENT_APP_FILE;
    var PORT = 3000;
    var SERVER = "localhost";
    var SERVER_URI = "http://" + SERVER + ":" + PORT + "/";


    var server;
    var fs = require("fs");
    var request = require("request");
    var util = require("../../lib/server_lib");
    var httpGet_lib = require("../../lib/spec_lib").httpGet;
    var httpRequest_lib = require("../../lib/spec_lib").httpRequest;
    var repo = require("../mongodb_repo").init("berlin-test");

    describe("Server", function(){
        var expectedData = "Hello world";
        server = require("../server").init({});
        var collections = server.routes.getCollections();

        beforeEach(function(){
            server = require("../server").init({
                port: PORT,
                mainFile: TEST_FILE,
                notFoundFile: TEST_FILE_404,
                assetsDir: ASSETS_DIR,
                clientAppDir: CLIENT_APP_DIR,
                repo: repo});
        });
        afterEach(function(done){
            tearDownFiles([TEST_FILE,
                    TEST_FILE_404,
                    TEST_FILE_ASSETS,
                    TEST_FILE_CLIENT_APP,
                    TEST_FILE_CLIENT_SPEC,
                    TEST_FILE_CLIENT_SPEC_FOLDER,
                    TEST_FILE_CLIENT_IMAGE_FOLDER]);

            if(server){
                try{
                    server.stop(function(){
                        done();
                    });
                }catch(ex){
                    done();
                }
            }
        });
        
        it("has a version property", function () {
            expect(server.version).toBeDefined();
        });
        it("has a port property", function(){
            expect(server.port).toMatch(/\d+/);
        });
        it("has a mainFile property", function(){
            expect(server.mainFile).toMatch(/\w\.html/);
        });
        it("has a notFoundFile property", function(){
            expect(server.notFoundFile).toMatch(/\w\.html/);
        });
        it("listen to given port", function(){
            spyOn(server.app, "listen");
            server.start(function(){
                expect(server.app.listen.mostRecentCall.args[0]).toBe(PORT);
            });
            expect(server.port).toBe(PORT);
        });
        it("throws an exception when stop is called before start", function(){
            expect(server.stop).toThrow();
        });
        it("uses express json, urlencoded and multipart modules", function(){
            var usesJson = false;
            var usesUrlEncoded = false;
            var usesMultipart = false;
            server.app.stack.forEach(function(element){
                if(element.handle.name === 'json') usesJson = true;
                if(element.handle.name === 'urlencoded') usesUrlEncoded = true;
                if(element.handle.name === 'multipart') usesMultipart = true;
            });
            expect((usesJson && usesUrlEncoded && usesMultipart)).toBeTruthy();
        });
        it("listens to a simple request", function(done){
            testURL(SERVER_URI, expectedData, done);
        });
        it("serves a file", function(done){
            var url = SERVER_URI + TEST_FILE;
            testURL(url, expectedData, done);
        });
        it("serves homepage", function(done){
            var url = SERVER_URI + "index.html";
            testURL(url, expectedData, done);
        });
        it("returns 404 page for every page but homepage", function(done){
            var url = SERVER_URI + "marglar";
            writeTestFile404With(expectedData);
            testURL(url, expectedData, done, 404);
        });
        it("serves assets", function(done){
            var url = SERVER_URI + "assets" + ASSET_FILE;
            writeTestFileAssetsWith(expectedData);
            testURL(url, expectedData, done);
        });
        it("serves client app files", function(done){
            var url = SERVER_URI + "client/" + CLIENT_APP_FILE;
            writeTestFileClientAppWith(expectedData);
            testURL(url, expectedData, done);
        });
        it("serves image files", function(done){
            var url = SERVER_URI + "client" + "/images/" + CLIENT_APP_FILE;
            writeTestFileClientImageDirAppWith(expectedData);
            testURL(url, expectedData, done);
        });
        it("serves image files on /images", function(done){
            var url = SERVER_URI + "images/" + CLIENT_APP_FILE;
            writeTestFileClientImageDirAppWith(expectedData);
            testURL(url, expectedData, done);
        });
        it("serves image not found file on /images when the file doesnt exists", function(done){
            var url = SERVER_URI + "images/" + 'testImage';
            
            spyOn(util, 'decodeUri').andCallThrough();
            httpGet(url, function(response, responseData){
                expect(util.decodeUri).toHaveBeenCalled();
                expect(response.statusCode).toBe(404);
                done();
            });
        });
        it("can post images", function(done){
            var path = "/image";
            var options = {
                path: path,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };
            spyOn(util, 'decodeUri').andCallThrough();
            httpRequest(options, {}, function(response, responseData){
                try{
                    expect(util.decodeUri).toHaveBeenCalled();
                    done();
                }catch(ex){
                    done(ex.message);
                }
            });
        });
        it("stores a image file", function(done){
            var url = SERVER_URI + "client/" + CLIENT_APP_FILE;
            var options = {
                hostname: 'localhost',
                port: 3000,
                path: "/image",
                method: 'POST',
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            };
            spyOn(util, 'decodeUri').andCallThrough();
            writeTestFileClientImageDirAppWith(expectedData);

            server.start(function(){
                var r = request.post(SERVER_URI + 'image', function(response, responseData){
                    try{
                        expect(util.decodeUri).toHaveBeenCalled();
                        fs.exists(TEST_FILE_CLIENT_IMAGE_FOLDER, function(exists){
                          expect(exists).toBeTruthy();
                          done();
                        });
                    }catch(ex){
                        done(ex.message);
                    }
                });
                var form = r.form();
                form.append('name', CLIENT_APP_FILE);
                form.append('image', fs.createReadStream(TEST_FILE_CLIENT_IMAGE_FOLDER));
            });
        });
        it("doesnt serve client spec folder files", function(done){
            var url = SERVER_URI + "client" + '/spec/' + CLIENT_APP_FILE;
            writeTestFile404With(expectedData);
            writeTestFileClientSpecDirAppWith(expectedData);
            testURL(url, expectedData, done, 404);
        });
        it("doesnt serve client spec files", function(done){
            var url = SERVER_URI + "client/" + CLIENT_SPEC_FILE;
            writeTestFile404With(expectedData);
            writeTestFileClientSpecWith(expectedData);
            testURL(url, expectedData, done, 404);
        });
        describe("Collections", function(){
            util.runCallbackForEachArrayItem(collections, function(collection){
                it("serves index.html for " + collection.name + " collection uris", function(done){
                    var url = SERVER_URI + collection.name;
                    testURL(url, expectedData, done);
                });
            });
            util.runCallbackForEachArrayItem(collections, function(collection){
                it("serves index.html for " + collection.name + " collection item uris", function(done){
                    var url = SERVER_URI + collection.name + "/0";
                    testURL(url, expectedData, done);
                });
            });
            util.runCallbackForEachArrayItem(collections, function(collection){
                it("serves index.html for " + collection.name + " collection item uris referencing names", function(done){
                    var url = SERVER_URI + collection.name + "/name";
                    testURL(url, expectedData, done);
                });
            });
            util.runCallbackForEachArrayItem(collections, function(collection){
                it("serves index.html for " + collection.name + " collection item uris referencing names with various words", function(done){
                    var url = SERVER_URI + collection.name + "/name%20with%20various%20words";
                    testURL(url, expectedData, done);
                });
            });
            util.runCallbackForEachArrayItem(collections, function(collection){
                it("serves index.html for " + collection.name + " collection item uris referencing names with special chars", function(done){
                    var url = SERVER_URI + collection.name + "/ÍóÚú_á ÁñÑéÉ pÓ-íüÜ";
                    testURL(url, expectedData, done);
                });
            });
            util.runCallbackForEachArrayItem(collections, function(collection){
                it("serves index.html for " + collection.name + " collection new item uris", function(done){
                    var url = SERVER_URI + collection.name + "/new";
                    testURL(url, expectedData, done);
                });
            });
            util.runCallbackForEachArrayItem(collections, function(collection){
                it("serves index.html for " + collection.name + " collection delete item uris", function(done){
                    var url = SERVER_URI + collection.name + "/0/delete";
                    testURL(url, expectedData, done);
                });
            });
            util.runCallbackForEachArrayItem(collections, function(collection){
                it("serves index.html for " + collection.name + " collection delete item uris referencing names", function(done){
                    var url = SERVER_URI + collection.name + "/name/delete";
                    testURL(url, expectedData, done);
                });
            });
            util.runCallbackForEachArrayItem(collections, function(collection){
                if(collection.child === undefined) return;
                
                it("serves index.html for " + collection.name + " collection item childCollection(" + collection.child + ") uris", function(done){
                    var url = SERVER_URI + collection.name + "/0/" + collection.child;
                    testURL(url, expectedData, done);
                });
            });
            util.runCallbackForEachArrayItem(collections, function(collection){
                if(collection.child === undefined) return;
                
                it("serves index.html for " + collection.name + " collection item childCollection(" + collection.child + ") uris referencing names", function(done){
                    var url = SERVER_URI + collection.name + "/name/" + collection.child;
                    testURL(url, expectedData, done);
                });
            });
            util.runCallbackForEachArrayItem(collections, function(collection){
                if(collection.child === undefined) return;
                if(collection.results === undefined) return;
                
                it("serves index.html for results of " + collection.name + " collection item  uris referencing names", function(done){
                    var url = SERVER_URI + collection.name + "/name/" + collection.results;
                    testURL(url, expectedData, done);
                });
            });
        });
        
        function testURL(url, expectedData, done, expectedStatusCode){
            writeTestFileWith(expectedData);
            spyOn(util, 'decodeUri').andCallThrough();
            httpGet(url, function(response, responseData){
                expect(util.decodeUri).toHaveBeenCalled();
                expect(response.statusCode).toBe(expectedStatusCode || 200);
                expect(responseData).toBe(expectedData);
                done();
            });
        }

        function httpGet(url, callback){
            server.start(
                function(){
                    httpGet_lib(url, callback);
                }
            );
        }
        function httpRequest(options, data, callback){
            var innerOptions = {
                hostname: options.hostname || SERVER,
                port: options.port || PORT,
                path: options.path,
                method: options.method,
                headers: options.headers || {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };
            server.start(
                function(){
                    httpRequest_lib(innerOptions, data, callback);
                }
            );
        }
        function writeTestFileWith(expectedData){
            fs.writeFileSync(TEST_FILE, expectedData);
        }
        function writeTestFile404With(expectedData){
            fs.writeFileSync(TEST_FILE_404, expectedData);
        }
        function writeTestFileAssetsWith(expectedData){
            fs.writeFileSync(TEST_FILE_ASSETS, expectedData);
        }
        function writeTestFileClientAppWith(expectedData){
            fs.writeFileSync(TEST_FILE_CLIENT_APP, expectedData);
        }
        function writeTestFileClientImageDirAppWith(expectedData){
            fs.writeFileSync(TEST_FILE_CLIENT_IMAGE_FOLDER, expectedData);
        }
        function writeTestFileClientSpecDirAppWith(expectedData){
            fs.writeFileSync(TEST_FILE_CLIENT_SPEC_FOLDER, expectedData);
        }
        function writeTestFileClientSpecWith(expectedData){
            fs.writeFileSync(TEST_FILE_CLIENT_SPEC, expectedData);
        }
    });
    function tearDownFiles(files){
        var i;
        for(i=0;i<files.length;i++){
            var file = files[i];
            if(fs.existsSync(file)){
                fs.unlinkSync(file);
                expect(fs.existsSync(file)).toBeFalsy();
            }
        }
    }
})();