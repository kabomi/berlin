// test on digital ocean
/*global describe, it, expect, spyOn, beforeEach, fs, xit, done, jasmine, afterEach, xdescribe */

"use strict";

(function(){

    var httpGet = require("../../lib/spec_lib").httpGet;
    var serverUri = "http://95.85.2.195/";

    xdescribe("droplet digital ocean tests", function(){
        it("serves homepage", function (done) {
            var marker = "This is the HomePage";

            function onResponse(response, responseData){
                var foundPage = (responseData.indexOf(marker) !== -1);
                expect(foundPage).toBeTruthy();
                done();
            }
            function onError(error, msg){
                done("home page should contain the following marker: " + marker);
            }

            httpGet(serverUri, onResponse, onError);
        });
    });
    
})();