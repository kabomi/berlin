/* jshint -W003 */

"use strict";

(function(){
	var self = {};
    var util = require('../lib/server_lib');
    var fs = require("fs");
    var http = require("http");
    var _ = require("underscore");

    function init(options){
        self.repo = options.repo;


        self.getJsonDataRouteHandler = getJsonDataRouteHandler;
        self.deleteJsonDataRouteHandler = deleteJsonDataRouteHandler;
        self.putOrPostJsonDataRouteHandler = putOrPostJsonDataRouteHandler;
        self.postJsonDataRouteHandler = postJsonDataRouteHandler;

        return self;
    }

    function getJsonDataRouteHandler(req, res){
        util.log("Attend app.get /data\/.*/");
        var match = getMatchObjectFromUrl(req.url);

        if (match.noCollectionName) return;
        var collectionName = match.getCollectionName();
        var itemValue = match.getItemValue();

        res.writeHead(200, {'Content-Type': 'application/json'});
        if (match.itemValue){
            self.repo.getCollectionItem(collectionName, itemValue, function(response){
                util.log("getCollectionItem RESPONSE:" + response);

                updateChildCollectionDataResponse(response, function(updatedResponse){
                    util.log("updated RESPONSE:" + updatedResponse);
                    res.end(updatedResponse);
                });
            });
        }else{
            self.repo.getCollection(collectionName, function(response){
                util.log("getCollection RESPONSE:" + response);
                res.end(response);
            });
        }
    }
    function getMatchObjectFromUrl(url){
        var self = {};

        var matchCollection = /^\/data\/(\w*[^\/])\/?/;
        var matchItem = /^\/data\/.*[^\/]\/(.*[^\/])\/?/;
        var matchNew = /^\/data\/.*[^\/]\/(new)\/?/;
        var matchExtra = /^\/data\/\w*[^\/]\/(.*[^\/])\/(\w*[^\/])\/?/;

        var decodedUrl = util.decodeUri(url);
        var collectionMatches = decodedUrl.match(matchCollection);
        var itemMatches = decodedUrl.match(matchItem);
        var newMatches = decodedUrl.match(matchNew);
        var extraMatches = decodedUrl.match(matchExtra);
        var newIsMatching = false;
        var collectionNameMatch, itemValueMatch, extraValueMatch;

        if (collectionMatches) collectionNameMatch = collectionMatches[1];
        if (itemMatches) itemValueMatch = itemMatches[1];
        if (newMatches) newIsMatching = true;
        if (extraMatches){
            extraValueMatch = extraMatches[2];
            itemValueMatch = extraMatches[1];
        }

        self.noCollectionName = (collectionNameMatch === undefined);
        self.itemValue = (itemValueMatch !== undefined);
        self.new = newIsMatching;
        self.collectionNameAndItemValue = (!self.noCollectionName && self.itemValue);

        self.getCollectionName = function(){
            return collectionNameMatch;
        };
        self.getItemValue = function(){
            return itemValueMatch;
        };
        self.getExtraValue = function(){
            return extraValueMatch;
        };

        return self;
    }
    function deleteJsonDataRouteHandler(req, res){
        try{
            util.log("Attend app.delete /data\/.*/");
            var match = getMatchObjectFromUrl(req.url);
            var collectionName = match.getCollectionName();
            var itemValue = match.getItemValue();

            if (match.collectionNameAndItemValue){
                res.writeHead(200, {'Content-Type': 'application/json'});
                util.printObj(req.body, "body");
                var parsedObject = req.body;
                self.repo.removeCollectionItem(collectionName, itemValue, onResponse);
            }else{
                util.log("doesn't match: collectionNameMatch && itemValueMatch");
                res.end('Error');
            }
        }catch (ex){
            util.log("error:" + ex.message + "\n" + ex.stack);
            res.end('Error');
        }

        function onResponse(response){
            if (response){
                util.log("removeCollectionItem RESPONSE:" + response);
                res.end(response);
            }else{
                res.end("Could not save requested data: " + req.body);
            }
        }
    }
    function postJsonDataRouteHandler(req, res){
        util.log("app.post");
        res.json({user: 'test'});
        res.end();
    }
    function putOrPostJsonDataRouteHandler(req, res){
        try{
            util.log("Attend app." + req.method + " /data\/.*/");
            var match = getMatchObjectFromUrl(req.url);
            var collectionName = match.getCollectionName();
            var itemValue = match.getItemValue();

            if (!match.collectionNameAndItemValue){
                util.log("doesn't match: collectionName && itemValueMatch");
                res.end('Error');
                return;
            }

            res.writeHead(200, {'Content-Type': 'application/json'});
            util.printObj(req.body, "req.body");
            var parsedObject = req.body;
            var itemValueToCheck = itemValue;
            if (match.new){
                itemValueToCheck = parsedObject.name;
            }
            self.repo.getCollectionItem(collectionName, itemValueToCheck, function(jsonString){
                var item = JSON.parse(jsonString);
                if (checkInvalidName(item.name, parsedObject.name)){
                    util.log('Trying to update/insert item with invalid name: ' + parsedObject.name);
                    res.end("Error");
                    return;
                }
                if (item.name === undefined){
                    self.repo.setCollectionItem(collectionName, parsedObject, function(response){

                        onResponse(response);
                    });
                    return;
                }

                checkInvalidNameWithinCollection(collectionName, item.name, parsedObject.name, function(invalid){
                    if (invalid){
                        util.log('Trying to update name already taken: ' + parsedObject.name);
                        res.end('Error');
                        return;
                    }
                    self.repo.setCollectionItem(collectionName, parsedObject, function(response){

                        onResponse(response);
                    });
                });
            });
        }catch (ex){
            util.log("error:" + ex.message + "\n" + ex.stack);
            res.end('Error');
        }
        function checkInvalidName(oldValue, newValue){
            if (newValue === undefined ||
                newValue === null ||
                newValue.toLowerCase() === 'new' ||
                newValue === ''){
                return true;
            }
        }
        function onResponse(response){
            if (response){
                util.log("removeCollectionItem RESPONSE:" + response);
                res.end(response);
            }else{
                res.end("Could not save requested data: " + req.body);
            }
        }
        function checkInvalidNameWithinCollection(collectionName, oldValue, newValue, callback){
            self.repo.getCollection(collectionName, function(jsonString){
                var collection = JSON.parse(jsonString);
                var invalidNameFound = false;
                if (util.isNotArray(collection)){
                    callback(false);
                    return;
                }
                collection.forEach(function(item){
                    var itemName = item.name.toLowerCase();
                    if (itemName === newValue.toLowerCase() &&
                        itemName !== oldValue.toLowerCase()){

                        invalidNameFound = true;
                    }
                });
                if (invalidNameFound){
                    util.log('No se puede utilizar el nombre ' + newValue + ' porque ya esta siendo utilizado');
                }
                callback(invalidNameFound);
            });
        }
    }

    function updateChildCollectionDataResponse(jsonString, onUpdateResponse){
        var item = JSON.parse(jsonString);
        var collectionName , childCollection;
        var childCollectionFound = false;
        _.each(item, function(value, key){
            if (!childCollectionFound &&
                _.has(item, key) &&
                util.isArray(value)){

                    collectionName = key;
                    childCollection = value;
                    childCollectionFound = true;
            }
        });
        if (childCollectionFound){
            self.repo.getCollection(collectionName, function(response){
                var collection = JSON.parse(response);
                var updatedItemCollection = [];
                collection.forEach(function(ele){
                    var index = util.indexOfArrayItemById(childCollection, ele);
                    var found = (index > -1);
                    if (found){
                        ele.value = childCollection[index].value;
                        updatedItemCollection.push(ele);
                    }
                });
                item[collectionName] = updatedItemCollection;
                onUpdateResponse(JSON.stringify(item));
            });
        }else{
            onUpdateResponse(jsonString);
        }
    }

    exports.init = init;
})();
