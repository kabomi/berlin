/* jshint -W003 */

(function(){

    "use strict";
    
    var self = {};
    var dbName;
    var MongoClient = require('mongodb').MongoClient;
    var ObjectID = require('mongodb').ObjectID;
    var format = require('util').format;
    var util = require('../lib/server_lib');
    var HOST = process.env.MONGODB_HOST ? process.env.MONGODB_HOST : 'localhost';
    var PORT = process.env.MONGODB_PORT ? process.env.MONGODB_PORT : 27017;
    var URI;

    function init(dbName){
        dbName = dbName;
        URI = format("mongodb://%s:%s/%s?w=1", HOST, PORT, dbName);

        function getCollection(collectionName, callback){
            _getCollection(collectionName, callback);
        }
        function _getCollection(collectionName, callback, filter){
            var result = [];
            findItemsFromCollection(collectionName, onItemFound, filter);
            function onItemFound(db, item){
                if (item === null){
                    callback(JSON.stringify(result));
                    db.close();
                }else{
                    item.id = item._id;
                    util.printObj(item, "GetCollection:item");
                    result.push(item);
                }
            }
        }
        function findItemsFromCollection(collectionName, callback, filter){
            MongoClient.connect(URI, function(err, db){
                if (err) throw "Error connecting to mongo server";

                var collection = db.collection(collectionName);
                collection.count(function(err, count) {
                    collection.find(filter || {}).each(function(err, item){
                        callback(db, item, collection);
                    });
                });
            });
        }

        function getCollectionByFilter(collectionName, filter, callback){
            _getCollection(collectionName, callback, filter);
        }

        function getCollectionItem(collectionName, itemValue, callback){
            var result = {};
            if (itemValue === undefined){
                callback(JSON.stringify({error: true}));
                return;
            }
            var filter = getFilterForItemValue(itemValue);
            findItemsFromCollection(collectionName, onItemFound, filter);
            function onItemFound(db, item){
                if (item === null){
                    db.close();
                    callback(JSON.stringify(result));
                }else{
                    item.id = item._id;
                    util.printObj(item, "GetCollectionItem:item");
                    result = item;
                }
            }
        }
        function getFilterForItemValue(itemValue){
            var _idValue = getObjectIdFromStringValueOrValue(itemValue);
            var filterArray = [ { _id: _idValue },
                    { id: itemValue },
                    { name: itemValue}
            ];
            var filter = {$or: filterArray};

            return filter;
        }
        function getObjectIdFromStringValueOrValue(value){
            if (value !== undefined &&
                typeof(value) === "string" &&
                value.length === 24){

                return new ObjectID.createFromHexString(value);
            }
            return value;
        }

        function setCollectionItem(collectionName, item, callback){
            insertOrReplaceItemToCollection(collectionName, item, function(db, doc, opResult){
                db.close();
                callback(JSON.stringify(doc), opResult);
            });
        }
        function insertOrReplaceItemToCollection(collectionName, item, callback){
            MongoClient.connect(URI, function(err, db){
                if (hasNotValidIds(item)){
                    onError(db);
                    return;
                }
                var filter = setFilterWithValidIds(item);
                var collection = db.collection(collectionName);
                if (hasNotValidFilterFields(item)){
                    doInsert(collection, item, db);
                    return;
                }
                collection.find(filter).count(function(err, count) {
                    if (err || count > 1){
                        onError(db);
                        return;
                    }
                    if (item._id !== undefined){
                        item._id = getObjectIdFromStringValueOrValue(item._id);
                    }
                    if (count === 1){
                        collection.findAndModify(filter,{}, item, {}, function(err, doc) {
                            var response = err || doc;
                            callback(db, response, {updated: true});
                        });
                        return;
                    }
                    doInsert(collection, item, db);
                });
            });
            function doInsert(collection, item, db){
                collection.insert(item, function(err, doc){
                    var response = err || doc[0];
                    response.id = response._id;
                    callback(db, response, {inserted: true});
                });
            }
            function onError(db){
                callback(db, {}, {error: true});
            }
        }
        function hasNotValidIds(item){
            if (item  === undefined ||
                typeof(item) !== 'object'){

                return true;
            }
            for (var name in item){
                if (item.hasOwnProperty(name)){
                    var field = item[name];
                    if (typeof(field) === 'undefined' ||
                        field === null){

                        return true;
                    }
                }
            }
            return false;
        }
        function setFilterWithValidIds(item){
            if (item  === undefined ||
                typeof(item) !== 'object'){

                return null;
            }
            var validFilterArray = [];
            var validFilterFields = getValidFilterFields();
            validFilterFields.forEach(function(name){
                var fieldValue = item[name];
                var validFilter = {};
                if (fieldValue !== undefined &&
                    fieldValue !== null){

                    if (name === '_id'){
                        validFilter[name] = getObjectIdFromStringValueOrValue(fieldValue);
                    }else{
                        validFilter[name] = fieldValue;
                    }
                    
                    validFilterArray.push(validFilter);
                }else{
                    if (name === '_id' &&
                        item.id !== undefined &&
                        item.id !== null){

                        validFilter[name] = getObjectIdFromStringValueOrValue(item.id);
                        validFilterArray.push(validFilter);
                    }
                }
            });
            return {$or: validFilterArray};
        }
        function getValidFilterFields(){
            return ['id', '_id', 'name'];
        }
        function hasNotValidFilterFields(item){
            var validFilterFields = getValidFilterFields();
            var validFilterFieldFound = false;
            for (var name in item){
                if (item.hasOwnProperty(name)){
                    if (validFilterFields.indexOf(name) > -1){
                        validFilterFieldFound = true;
                    }
                }
            }
            return !validFilterFieldFound;
        }
    
        function removeCollectionItem(collectionName, itemValue, callback){
            if (itemValue === undefined){
                callback(JSON.stringify({error: true}));
                return;
            }
            var filter = getFilterForItemValue(itemValue);
            findItemsFromCollection(collectionName, onItemFound, filter);
            function onItemFound(db, item, collection){
                if (item !== null){
                    collection.remove(item, function(err, result) {
                        db.close();
                        callback(JSON.stringify({deleted:result}));
                    });
                }
            }
        }


        self.getCollection = getCollection;
        self.getCollectionByFilter = getCollectionByFilter;
        self.getCollectionItem = getCollectionItem;
        self.setCollectionItem = setCollectionItem;
        self.removeCollectionItem = removeCollectionItem;
        return self;
    }

    exports.init = init;
}());