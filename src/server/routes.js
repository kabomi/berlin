"use strict";

(function(){

    var self = {};
    var util = require('../lib/server_lib');

    function init(){

        self.isMainFileRoute = isMainFileRoute;
        self.getCollections = getCollections;

        return self;
    }

    function getCollections(){
        var collections = [
            /*{   name: 'grupos',
                child: 'fichas'
            },
            {   name: 'fichas',
                child: 'caracteristicas'
            },
            {   name: 'eventos',
                child: 'caracteristicas',
                results: 'clasificacion'
            },
            {   name: 'caracteristicas'
            },
            {   name: 'lesiones'
            }*/
        ];
        return collections;
    }

    var routes = [];

    function isMainFileRoute(url){
        initRoutes();
        var i;
        util.log("URL TO TEST:" + url);
        for (i=0;i<routes.length;i++){
            if (routes[i].test(url)){
                util.log("url:" + url + ":::" + routes[i].toString());
                return true;
            }
        }
        return false;
    }
    function initRoutes () {
        if (routes.length > 0) return;

        routes.push(/^\/$/i);
        routes.push(/^\/index.html\/?$/i);
        util.runCallbackForEachArrayItem(self.getCollections(), function(collection){
            var regexp = new RegExp("^\/" + collection.name + "\/?$", "i");
            var itemName = "[\\wáÁéÉíÍóÓúÚñÑüÜ -_]+";
            routes.push(regexp);

            regexp = new RegExp("^\/" + collection.name + "\/new\/?$", "i");
            routes.push(regexp);

            regexp = new RegExp("^\/" + collection.name + "\/(" + itemName + "\\s?)+\/?$", "i");
            routes.push(regexp);

            regexp = new RegExp("^\/" + collection.name + "\/(" + itemName + "\\s?)+\/delete\/?$", "i");
            routes.push(regexp);

            if (collection.child !== undefined){
                regexp = new RegExp("^\/" + collection.name + "\/(" + itemName + "\\s?)+\/" + collection.child + "\/?$", "i");
                routes.push(regexp);

                if (collection.results !== undefined){
                    regexp = new RegExp("^\/" + collection.name + "\/(" + itemName + "\\s?)+\/" + collection.results + "\/?$", "i");
                    routes.push(regexp);
                }
            }
        });
    }

    exports.init = init;
})();
