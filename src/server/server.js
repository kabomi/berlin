"use strict";

(function(){
    var VERSION = 0.1;
    var self = {};
    var express;
    var app;
    var _server;
    var util = require('../lib/server_lib');
    var fs = require("fs");
    var http = require("http");

    self.version = VERSION;
    function init(options){
        express = require('express');
        app = express();
        app.use(express.json());
        app.use(express.urlencoded());
        app.use(express.multipart({uploadDir: options.clientAppDir + 'images'}));
        self.app = app;
        self.port = options.port;
        self.mainFile = options.mainFile;
        self.notFoundFile = options.notFoundFile;
        self.assetsDir= options.assetsDir;
        self.clientAppDir = options.clientAppDir;
        self.repo = options.repo;
        self.handlers = require('./handlers').init({repo: self.repo});
        self.routes = require('./routes').init();

        initRoutes();

        self.start = function(callback){
            if (util.isNumber(self.port)){
                _server = app.listen(self.port, callback);
            }else{
                throw "Invalid port";
            }
        };
        self.stop = function(callback){
            if (_server){
                try{
                    _server.close(callback);
                }catch(ex){
                    throw ex;
                }
            }else{
                throw "Server must be started";
            }
        };
        self.get = function(url, callback){
            try{
                return http.get(url, callback);
            }catch (ex){
                util.log("Error:" + ex.message);
                throw ex;
            }
        };

       
        return self;
    }
    function initRoutes(){
        var handlers = self.handlers;

        var jsonDataRoute = /data\/.*/;
        app.get(jsonDataRoute, handlers.getJsonDataRouteHandler);
        app.delete(jsonDataRoute, handlers.deleteJsonDataRouteHandler);
        app.put(jsonDataRoute, handlers.putOrPostJsonDataRouteHandler);
        app.post(jsonDataRoute, handlers.putOrPostJsonDataRouteHandler);

        var allRoute = /.*/;
        app.get(allRoute, serverHandler);
        var imageRoute = /image\/?.*/;
        app.post(imageRoute, imageHandler);
        var jsonDataLoginRoute = '/login';
        app.post(jsonDataLoginRoute, handlers.postJsonDataRouteHandler);
    }
    function serverHandler(req, res){
        try{
            var url = util.decodeUri(req.path);
            util.log("get url:" + url);
            if (url.indexOf('/spec/') > -1 ||
                url.indexOf('_spec.js') > -1){
                sendFileNotFound(req, res);
                return;
            }

            if (self.routes.isMainFileRoute(url)){
                util.log("send mainfile:" + url);
                sendFile(self.mainFile, req, res);
                return;
            }
            if (url.indexOf('/assets/') > -1){
                url = url.replace('assets/', self.assetsDir);
                util.log("send assetsDir: " + url);
            }else{
                if (url.indexOf('/client/') > -1){
                    url = url.replace('client/', self.clientAppDir);
                    util.log("send clientAppDir: " + url);
                }
            }
            if (url.indexOf('/images/') === 0){
                url = url.replace('images/', self.clientAppDir + 'images/');
                util.log("send clientAppImageDir: " + url);
                sendFileImage(url.slice(1), req, res);
                return;
            }

            sendFile(url.slice(1), req, res);
        }catch (ex){
            util.log("Error:" + ex.message);
            res.end();
        }
    }
    function sendFileImage(url, req, res){
        handleFile(url, req, res, sendFileImageNotFound);
    }
    function sendFile (url, req, res){
        handleFile(url, req, res, sendFileNotFound);
    }
    function sendFileImageNotFound(req, res){
        res.statusCode = 404;
        handleFile(self.clientAppDir + 'assets/images/' + 'not_found.png', req, res);
    }
    function sendFileNotFound(req, res){
        res.statusCode = 404;
        handleFile(self.notFoundFile, req, res);
    }
    function handleFile(url, req, res, errorHandler){
        var file = fs.createReadStream(url);
        file.on('error', function(err){
            if (errorHandler){
                errorHandler(req, res);
            }else{
                res.end();
            }
        });
        file.on('data', function(data){
            res.write(data);
        });
        file.on('end', function(){
            res.end();
        });
    }

    function imageHandler(req, res){
        var url = util.decodeUri(req.path);
        util.log("post url:" + url);
        var image = req.files.image;
        fs.readFile(image.path, function (err, data) {
          var newPath = self.clientAppDir + "/images/" + req.body.name;
          fs.rename(image.path, newPath, function(err) {
                if (err) throw err;

                util.log('File uploaded!');
                res.redirect('back');
            });
        });
    }


    exports.init = init;
})();
