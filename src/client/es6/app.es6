

berlin = berlin || {};

(berlin) =>{
    "use strict";

    if (typeof window !== "undefined" && window !== null) {
        window.onpopstate = function(e){
            console.log(`popState: ${document.location.pathname}`);
            crossroads.parse(document.location.pathname);
            e.preventDefault();
        }
    }

    let App = () =>{
        let Factory = berlin.Factory;
        if (typeof require !== "undefined" && require !== null) {
            // Factory = require("factory");
        }
        let self = Factory.createApp();
        let util = berlin.Util;


        self.routeHandlers = {home: () => {resolveHomeView();}};
        self.routes = {};

        function createAndInitializeInteractors(){
            util.runCallbackForEachArrayItem(self.collectionNames, (name) =>{
                createMethod = `create${util.capitalizeFirstLetter(name)}Interactor`;
                interactor = self[name + 'Interactor'] = Factory[createMethod](self);
                interactor.initialize();
                interactor.onNavigateToPage(navigateToPage);
                self.initButtons[name].onClick =  interactor.handleShowCollection;
            });
        }

        function initialize(){
            resolveHomeView();
            addRoutes();

            createAndInitializeInteractors();

            crossroads.ignoreState = true;
            self.backButton.onClick = onGoBack;
            self.homeButton.onClick = onGoHome;
            if (util.isNotFiringOnPopStateEventOnLoad()){
                crossroads.parse(document.location.pathname);
            }
        }
        function resolveHomeView(){
            /* Home view */
        }
        function onGoBack(){
            self.history.popState();
        }
        function onGoHome(){
            navigateToPage('/', 'HOME');
            self.routeHandlers.home();
        }
        function addRoutes(){
            self.routes.home =    crossroads.addRoute('/',        self.routeHandlers.home);
        }

        function navigateToPage(path, title){
            self.history.pushState(path, title);
        }


        self.initialize = initialize;
        self.navigateToPage = navigateToPage;



        return self;
    }

    if (typeof exports !== "undefined" && exports !== null) {
        module.exports.App = App;
    }
    berlin.App = App;

}(berlin)
