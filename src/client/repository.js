var berlin = berlin || {};

(function(){
    "use strict";
    function Repository(path){
        var self = this;
        var util = berlin.Util;

        var dataPath = path || '/data';
        self.collections = [];

        function getCollection(type, callback){
            var collection = [];
            self.collections[type] = collection;
            self.getData(dataPath + '/' + type, function(uri, jsonResult){
                if (jsonResult &&
                    jsonResult.length > 0){

                    self.collections[type] = jsonResult;
                }
                callback(uri, jsonResult);
            });
        }
        function getCollectionItem(type, item, callback){
            var itemId;
            if (typeof(item) === 'object' || item.id !== undefined){
                itemId = item.id;
            }else{
                itemId = item;
            }
            self.getData(dataPath + '/' + type + '/' + itemId, function(uri, jsonResult){
                if(jsonResult &&
                    jsonResult.id !== undefined){

                    if(!self.collections[type]) self.collections[type] = [];

                    util.deleteItemFromArrayById(self.collections[type], jsonResult.id);
                    self.collections[type].push(jsonResult);
                    util.log("self.collections[type]:" + JSON.stringify(self.collections[type]));
                }
                callback(uri, jsonResult);
            });
        }
        function updateCollectionItem(type, item, callback){
            var itemId = item.id;
            self.putData(dataPath + '/' + type + '/' + itemId, item, function(uri, jsonResult){
                if (jsonResult){
                    util.insertItemToArray(self.collections[type], item);
                }
                callback(uri, jsonResult);
            });
        }
        function deleteCollectionItem(type, item, callback){
            var itemId = item.id;
            util.deleteItemFromArrayById(self.collections[type], itemId);
            self.deleteData(dataPath + '/' + type + '/' + itemId, item, callback);
        }
        function getResults(type, itemId, callback){
            var results = 'clasificacion';
            self.getData(dataPath + '/' + type + '/' + itemId + '/' + results, function(uri, jsonResult){
                if(jsonResult &&
                    util.isArray(jsonResult)){

                    var collectionItem = util.getItemFromArrayById(self.collections[type], itemId);
                    collectionItem.results =jsonResult;
                    util.log("collectionItem.results:" + JSON.stringify(collectionItem));
                }
                callback(uri, jsonResult);
            });
        }
        function getData(uri, callback){
            function success(jsonResult){
                callback(uri, jsonResult);
            }
            function error(){
                callback(uri, null);
            }
            util.ajaxGet(uri, success, error);
        }
        function putData(uri, data, callback){
            function success(jsonResult){
                callback(uri, jsonResult);
            }
            function error(){
                callback(uri, null);
            }
            util.ajaxPut(uri, data, success, error);
        }
        function deleteData(uri, data, callback){
            function success(jsonResult){
                callback(uri, jsonResult);
            }
            function error(){
                callback(uri, null);
            }
            util.ajaxDelete(uri, data, success, error);
        }

        
        self.getCollection = getCollection;
        self.getCollectionItem = getCollectionItem;
        self.updateCollectionItem = updateCollectionItem;
        self.deleteCollectionItem = deleteCollectionItem;
        self.getResults = getResults;
        self.getData = getData;
        self.putData = putData;
        self.deleteData = deleteData;

        return self;
    }
    berlin.Repository = Repository;
})(berlin);