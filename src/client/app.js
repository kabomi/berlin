

(function(exports){
    "use strict";

    window.onpopstate = function(e){
        console.log("popState:" + document.location.pathname);
        crossroads.parse(document.location.pathname);
        e.preventDefault();
    };

    function App(){
        var Factory = exports.Factory;
        var self = Factory.createApp();
        var util = exports.Util;


        self.routeHandlers = {
            home:    function(){ resolveHomeView();}
        };
        self.routes = {};

        function createAndInitializeInteractors(){
            util.runCallbackForEachArrayItem(self.collectionNames, function(name){
                var createMethod = 'create' + util.capitalizeFirstLetter(name) + 'Interactor';
                var interactor = self[name + 'Interactor'] = Factory[createMethod](self);
                interactor.initialize();
                interactor.onNavigateToPage(navigateToPage);
                self.initButtons[name].onClick =  interactor.handleShowCollection;
            });
        }

        function initialize(){
            resolveHomeView();
            addRoutes();

            createAndInitializeInteractors();

            crossroads.ignoreState = true;
            self.backButton.onClick = onGoBack;
            self.homeButton.onClick = onGoHome;
            if (util.isNotFiringOnPopStateEventOnLoad()){
                crossroads.parse(document.location.pathname);
            }
        }
        function resolveHomeView(){
            /* Home view */
        }
        function onGoBack(){
            self.history.popState();
        }
        function onGoHome(){
            navigateToPage('/', 'HOME');
            self.routeHandlers.home();
        }
        function addRoutes(){
            self.routes.home =    crossroads.addRoute('/',        self.routeHandlers.home);
        }

        function navigateToPage(path, title){
            self.history.pushState(path, title);
        }


        self.initialize = initialize;
        self.navigateToPage = navigateToPage;

        return self;
    }

    exports.App = App;
})(typeof(exports) === 'string'? exports : berlin = berlin || {});
