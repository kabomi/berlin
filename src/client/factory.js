
var berlin = berlin || {};

(function(){
    "use strict";

    var util = berlin.Util;
    function Factory(){
        var self = this;
        var collectionNamesWithChildCollections = [/* collections with subcollections */];
        var collectionFieldNames = {
            //e.g. fichas: ['name', 'nick', 'grupo'],
        };
        var collectionFieldCaptions = {
            //e.g. fichas: ['Nombre', 'Alias', 'Grupo'],
        };
        self.collectionNamesWithChildCollections = collectionNamesWithChildCollections;

        function getCollectionTableFieldNames(collectionName){
            return collectionFieldNames[collectionName];
        }
        function getCollectionTableCaptionNames(collectionName){
            return collectionFieldCaptions[collectionName];
        }
        function getCollectionNames(){
            return [/*collections*/];
        }

        function createLogin(loginId){
            var self = {};
            var _user, _pass;

            self.setUser = function(userName){
                _user = userName;
            };
            self.setPass = function(password){
                _pass = password;
            };

            self.logIn = function(callback, errorCallback){
                util.ajaxPost('/login',
                    {
                        user: _user,
                        pass: _pass
                    },
                    function(data, textStatus, response){
                        util.log("LOGIN SUCCEDED!");
                        callback(response);
                    },
                    function(error){
                        util.log("LOGIN ERROR!");
                        errorCallback(error);
                    }
                );
            };

            self.nativeWidget = attachNativeWidget("login-box-button", "button");
            return self;
        }

        function createObject(id, tag){
            var self = {};
            self.nativeWidget = attachNativeWidget(id, tag);

            self.show = function(){
                self.nativeWidget.show();
            };
            self.hide = function(){
                self.nativeWidget.hide();
            };
            self.empty = function(){
                self.nativeWidget.empty();
            };
            self.getValue = function(){
                return self.nativeWidget.val();
            };
            self.setValue = function(value){
                return self.nativeWidget.val(value);
            };
            self.addClass = function(value){
                self.nativeWidget.addClass(value);
            };

            self.onClick = function(){/*event*/};

            self.bind = function(eventType, withFunction){
                self.nativeWidget.on(eventType, withFunction);
            };
            self.unbind = function(eventType){
                self.nativeWidget.unbind(eventType);
            };

            return self;
        }
        function attachNativeWidget(id, tag){
            return util.createDomControlIfNotExists(id, tag);
        }

        function createInitButtons(){
            var buttons = {};
            util.runCallbackForEachArrayItem(self.getCollectionNames(), function(name){
                buttons[name] = createButton(name);
            });
            buttons.show = function(){
                util.runCallbackForEachArrayItem(self.getCollectionNames(), function(name){
                    buttons[name].show();
                });
            };
            buttons.hide = function(){
                util.runCallbackForEachArrayItem(self.getCollectionNames(), function(name){
                    buttons[name].hide();
                });
            };

            return buttons;
        }
        function createButton(type, param){
            var self = createObject(type + '-button', 'button');
            self.unbind('click');
            self.bind('click', function(){self.onClick(param);});

            return self;
        }
        function createInputText(type){
            var self = createObject(type + '-input', 'input');
            return self;
        }
        function createList(type){
            var self = createObject(type + '-ul', "ul");
            return self;
        }
        function createSelect(type){
            var self = createObject(type + '-select', "select");
            return self;
        }
        function createCollectionPanels(){
            var panels = {};
            util.runCallbackForEachArrayItem(self.getCollectionNames(), function(name){
                panels[name] = createPanel(name);
                panels[name + '-edit'] = createPanel(name + '-edit');
                panels[name + '-addToCollection'] = createPanel(name + '-addToCollection');
            });
            panels.hide = function(){
                util.runCallbackForEachArrayItem(self.getCollectionNames(), function(name){
                    panels[name].hide();
                    panels[name + '-edit'].hide();
                    panels[name + '-addToCollection'].hide();
                });
            };

            return panels;
        }
        function createPanel(type){
            var self = createObject(type + '-panel', "div");
            return self;
        }
        function createTable(type){
            var self = createObject(type + '-table', "table");
            return self;
        }
        function createImage(type){
            var self = createObject(type + '-image', "image");
            return self;
        }
        function createForm(options){
            var self = createObject(options.id + '-form', "form");
            self.nativeWidget.prop('action', options.action);
            self.nativeWidget.prop('enctype', "multipart/form-data");

            return self;
        }


        function createBackPanel(){
            var self = createPanel('back');
            return self;
        }

        function createBackButton(){
            var self = createButton('back');
            return self;
        }

        function createHomeButton(){
            var self = createButton('home');
            return self;
        }


        function createNewButton(){
            var self = createButton('new');
            return self;
        }
        function createAddToCollectionButton(collectionName, param){
            var self = createButton(collectionName + '-addToCollection', param);
            return self;
        }

        function createHistory(){
            var self = {};

            function pushState(url, title, data){
                history.pushState(data, title, url);
            }
            function popState(){
                history.back();
            }
            function replaceState(url, title, data){
                history.replaceState(data, title, url);
            }

            self.pushState = pushState;
            self.popState =  popState;
            self.replaceState =  replaceState;

            return self;
        }

        function createRepo(path){
            var repo = new berlin.Repository(path);
            return repo;
        }


        function createApp(){
            return {
                repo : self.createRepo('/data'),
                history : self.createHistory(),
                collectionNames : self.getCollectionNames(),
                initButtons : self.createInitButtons(),
                collectionPanels : self.createCollectionPanels(),
                backPanel : self.createBackPanel(),
                backButton : self.createBackButton(),
                homeButton : self.createHomeButton()
            };
        }

        self.createLogin = createLogin;
        self.createInitButtons = createInitButtons;
        self.createCollectionPanels = createCollectionPanels;
        self.createBackPanel = createBackPanel;
        self.createAddToCollectionButton = createAddToCollectionButton;
        self.createBackButton = createBackButton;
        self.createHomeButton = createHomeButton;
        self.createNewButton = createNewButton;
        self.createHistory = createHistory;
        self.createRepo = createRepo;
        self.createTable = createTable;
        self.createImage = createImage;
        self.createForm = createForm;
        self.createPanel = createPanel;
        self.createInputText = createInputText;
        self.createList = createList;
        self.createSelect = createSelect;
        self.createButton = createButton;

        self.getCollectionNames = getCollectionNames;
        self.getCollectionTableFieldNames = getCollectionTableFieldNames;
        self.getCollectionTableCaptionNames = getCollectionTableCaptionNames;
        self.createApp = createApp;

        return self;
    }

    berlin.Factory = new Factory();
})(berlin);
