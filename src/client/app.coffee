

berlin = berlin || {}

(do(berlin) ->
    "use strict";

    window?.onpopstate = (e) ->
        console.log "popState:" + document.location.pathname
        crossroads.parse document.location.pathname
        e.preventDefault()

    App = () ->
        Factory = (require? "factory") || berlin.Factory
        self = Factory.createApp()
        util = berlin.Util


        self.routeHandlers = home: () -> resolveHomeView()
        self.routes = {}

        createAndInitializeInteractors = () ->
            util.runCallbackForEachArrayItem self.collectionNames, (name) ->
                createMethod = "create#{util.capitalizeFirstLetter(name)}Interactor"
                interactor = self[name + 'Interactor'] = Factory[createMethod](self)
                interactor.initialize()
                interactor.onNavigateToPage(navigateToPage)
                self.initButtons[name].onClick =  interactor.handleShowCollection

        initialize = () ->
            resolveHomeView()
            addRoutes()

            createAndInitializeInteractors()

            crossroads.ignoreState = true
            self.backButton.onClick = onGoBack
            self.homeButton.onClick = onGoHome
            if util.isNotFiringOnPopStateEventOnLoad()
                crossroads.parse(document.location.pathname)

        resolveHomeView = () ->
            # Home view
        onGoBack = () ->
            self.history.popState()
        onGoHome = () ->
            navigateToPage '/', 'HOME'
            self.routeHandlers.home()
        addRoutes = () ->
            self.routes.home =  crossroads.addRoute '/', self.routeHandlers.home

        navigateToPage = (path, title) ->
            self.history.pushState(path, title)


        self.initialize = initialize
        self.navigateToPage = navigateToPage

        return self

    exports?.App = App
    berlin.App = App
)
