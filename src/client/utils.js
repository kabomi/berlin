/* jshint -W083 */

var berlin = berlin || {};

(function(){
    "use strict";

    var self = {};

    function alertFunction(msg){
        if (berlin._DEBUG){
            self.log(msg);
        }else{
            alert(msg);
        }
    }
    function consoleLog(text){
        if (!berlin._DEBUG) return;

        console.log(text);
    }
    function savedData(text, isSaved){
        var saved = isSaved || true;
        var notSaved = !saved;
        self.log(text);
        $('#savedData').toggleClass("alert-success", saved)
            .toggleClass("alert-danger", notSaved)
            .find('i')
                .toggleClass("glyphicon-floppy-saved", saved)
                .toggleClass("glyphicon-floppy-removed", notSaved)
            .end()
            .fadeIn(1000).fadeOut(2000);

    }
    function notSavedData(text){
        self.savedData(text, false);
    }
    function printObj(obj, title){
        if (!berlin._DEBUG) return;

        self.log("[TITLE]:" + title);
        for (var name in obj){
            if (obj.hasOwnProperty(name)){
                var prop = obj[name];
                if (typeof(prop) !== 'function'){
                    self.log(name + ":" + prop + " [" + typeof(prop) + "]");
                }else{
                    self.log(name + ": [Function]");
                }
            }
        }
    }
    function prompt(options, callback){
        $.prompt(options.text, {
            title: options.title,
            buttons: options.buttons || { "Si, seguro": true, "No": false },
            submit: callback
        });
    }
    function isNotFiringOnPopStateEventOnLoad(){
        if (!isDefined(window) ||
            !isDefined(window.navigator)){

            return false;
        }
        var userAgent = window.navigator.userAgent.toLowerCase();
        var result = (userAgent.indexOf('firefox') > -1) ||
                (userAgent.indexOf('trident/6.0') > -1) || //ie 10
                (userAgent.indexOf('trident/7.0') > -1); //ie 11
        if (!result){
            return isChromeVersionGreaterThan34();
        }

        return true;
    }
    function isDefined(value){
        if (value !== null &&
            value !== undefined &&
            typeof(value) === 'object'){

            return true;
        }

        return false;
    }
    function isChromeVersionGreaterThan34(){
        var chrome = window.navigator.appVersion.match(/Chrome\/(\d+)\./);
        if (chrome !== null &&
            chrome[1] !== undefined &&
            !isNaN(parseInt(chrome[1],10)) &&
            parseInt(chrome[1],10) >= 35){

            return true;
        }
        return false;
    }
    function isNotArray(value){
        return !isArray(value);
    }
    function isArray(value){
        var result = (value !== undefined &&
            typeof value === 'object' &&
            typeof value.length === 'number' &&
            typeof value.splice === 'function' &&
            !(value.propertyIsEnumerable('length')));
        return result;
    }
    function indexOfArrayItemById(array, item){
        var index = -1;
        if (item.id === undefined || item.id === null) return;
        if (isNotArray(array)) return;

        array.forEach(function(ele, i){
            if (ele.id === item.id){
                index = i;
            }
        });
        return index;
    }
    function insertItemToArray(array, item){
        var index = indexOfArrayItemById(array, item);
        if (index >= 0){
            array.splice(index, 1, item);
        }else{
            array.push(item);
        }
    }
    function deleteItemFromArrayById(array, id){
        var index = -1;
        if (isNotArray(array))
            return;
        array.forEach(function(item, i){
            if (item.id === id){
                index = i;
            }
        });
        if (index >= 0){
            array.splice(index, 1);
        }
    }
    function deleteItemFromArrayByName(array, name){
        var index = -1;
        if (isNotArray(array))
            return;
        array.forEach(function(item, i){
            if (item.name === name){
                index = i;
            }
        });
        if (index >= 0){
            array.splice(index, 1);
        }
    }
    function decodeUri(value){
        if (typeof(value) === 'object' &&
                value !== null){
            return value;
        }
        return decodeURIComponent(value);
    }

    function ajaxGet(uri, successCallback, errorCallback){
        ajax(uri, 'GET', null, successCallback, errorCallback);
    }
    function ajaxPost(uri, data, successCallback, errorCallback){
        ajax(uri, 'POST', data, successCallback, errorCallback);
    }
    function ajaxPostFile(uri, data, successCallback, errorCallback){
        ajax(uri, 'POST', data, successCallback, errorCallback, true);
    }
    function ajaxPut(uri, data, successCallback, errorCallback){
        ajax(uri, 'PUT', data, successCallback, errorCallback);
    }
    function ajaxDelete(uri, data, successCallback, errorCallback){
        ajax(uri, 'DELETE', data, successCallback, errorCallback);
    }
    function ajax(uri, verb, data, successCallback, errorCallback, postFile){
        var options ={
            url: uri,
            data: data,
            type: verb,
            success: function(data) {
                var jsonResult = null;
                try{
                    if (typeof(data) === 'string' &&
                        !postFile){
                        jsonResult = JSON.parse(data);
                    }else{
                        jsonResult = data;
                    }
                }catch (ex){
                    self.log("Error parsing:" + ex);
                }
                successCallback(jsonResult);
            },
            error: function(){
                errorCallback(arguments);
            }
        };
        if (postFile){
            options.cache = false;
            options.contentType = false;
            options.processData = false;
            options.async = false;
        }
        $.ajax(options);
    }
    function createDomControlIfNotExists(id, type){
        var obj = $('#' + id);
        if (obj.length === 0){
            console.error('Object with id:' + id + ', should have been created');
            $('<' + type + '/>', {'id': id }).appendTo('body');
        }
        return $('#' + id);
    }
    function isInputValid(control){
        var result = $.formUtils.validateInput(control, {}, $.formUtils.defaultConfig());
        if (result === true){
            return true;
        }else{
            var hasNoValidation = (result === null);
            return hasNoValidation;
        }
    }

    function setImageForm(itemName, formTag){
        var formObj = berlin.Factory.createForm(formTag);
        formObj.empty();
        var form = formObj.nativeWidget;
        var imageInput = berlin.Factory.createImage(formTag.id);
        var imagePath= (itemName?'/images/' + itemName: '/assets/images/' + 'not_found.png');
        imageInput.nativeWidget.prop('src', imagePath);

        var image = $("<input name='image' type='file' class='form-control'/>");
        if (itemName === undefined){
            image.hide();
        }else{
            image.show();
        }
        image.unbind('change');
        image.on('change', function(){
            if (itemName === undefined) return;

            var formData = new FormData(form[0]);
            self.ajaxPostFile(formTag.action, formData, function(){
                imageInput.nativeWidget.prop('src', '/images/' + itemName + '?update');
            }, function(){
                self.log(arguments);
            });
        });
        var input = $("<input name='name' style='display:none'/>");
        input.val(itemName);
        image.appendTo(form);
        input.appendTo(form);
    }

    function applyFunctionToArrayItems(array, callFunction){
        for (var i in array){
            if (array.hasOwnProperty(i)){
                var arrayItem = array[i];
                callFunction.apply(null, [arrayItem]);
            }
        }
    }
    function runCallbackForEachArrayItem(array, callback){
        for (var i in array){
            if (array.hasOwnProperty(i)){
                var arrayItem = array[i];
                callback(arrayItem);
            }
        }
    }
    function applyFunctionToArguments(callFunction){
        callFunction.apply(null, butFirst.apply(null, arguments));
    }
    function butFirst(){
        return Array.prototype.slice.call(arguments, 1);
    }
    function capitalizeFirstLetter(str){
        if (typeof(str) === 'string' &&
            str.length > 0){
            return str.charAt(0).toUpperCase() + str.slice(1);
        }
        return str;
    }
    function getValueTrimmed(value){
        if (typeof(value) !== 'string') return value;

        return value.trim();
    }
    function validateInput(){
        var myLanguage = {
            badDate : 'Fecha incorrecta',
            badAlphaNumeric : 'Debe contener s&oacute;lo caracteres alfanum&eacute;ricos ',
            badAlphaNumericExtra: ' o "',
            spaces: '" o espacios ',
            badInt: 'No es un n&uacute;mero v&aacute;lido'
        };
        $.validate({
            language: myLanguage
        });
        $('input').bind('validation', function(evt, isValid) {
            self.log('Input "'+this.name+'" is ' + (isValid ? 'VALID' : 'NOT VALID'));
            if(!isValid){
                evt.stopPropagation();
            }
        });
    }
    function convertToDataTable($table){
        if ($table.dataTable === undefined) return;

        if ($.fn.DataTable.fnIsDataTable($table[0])) $table.dataTable().fnDestroy();
        $table.dataTable({
            "sPaginationType": "bs_normal",
            oLanguage: {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });
        var search_input = $table.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Buscar');
        search_input.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = $table.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
    }

    function observer(){
        var self = {}, subscribers = [];

        self.addSubscriber = function(subscriber){
            subscribers.push(subscriber);
        };
        self.publish = function(){
            var i=subscribers.length-1;
            while(i>=0){
                var subscriber = subscribers[i];
                subscriber.apply(subscriber, arguments);
                i--;
            }
        };


        return self;
    }
    

    self.alert = alertFunction;
    self.log = consoleLog;
    self.savedData = savedData;
    self.notSavedData = notSavedData;
    self.printObj = printObj;
    self.prompt = prompt;
    self.isNotFiringOnPopStateEventOnLoad = isNotFiringOnPopStateEventOnLoad;
    self.isDefined = isDefined;
    self.isNotArray = isNotArray;
    self.isArray = isArray;
    self.insertItemToArray = insertItemToArray;
    self.deleteItemFromArrayById = deleteItemFromArrayById;
    self.deleteItemFromArrayByName = deleteItemFromArrayByName;
    self.decodeUri = decodeUri;
    self.ajaxGet = ajaxGet;
    self.ajaxPost = ajaxPost;
    self.ajaxPostFile = ajaxPostFile;
    self.ajaxPut = ajaxPut;
    self.ajaxDelete = ajaxDelete;
    self.createDomControlIfNotExists = createDomControlIfNotExists;
    self.setImageForm = setImageForm;

    self.isInputValid = isInputValid;
    self.applyFunctionToArrayItems = applyFunctionToArrayItems;
    self.runCallbackForEachArrayItem = runCallbackForEachArrayItem;
    self.applyFunctionToArguments = applyFunctionToArguments;
    self.capitalizeFirstLetter = capitalizeFirstLetter;
    self.getValueTrimmed = getValueTrimmed;
    self.observer = observer;

    berlin.Util = self;
})(berlin);