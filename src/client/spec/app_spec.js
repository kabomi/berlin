/*global describe, it, expect, spyOn, beforeEach, fs, xit, done, jasmine, afterEach, xdescribe */

(function(SpecLib){
    "use strict";

    var util = berlin.Util;

    describe("karma", function(){

        it("runs tests properly", function () {
            expect(true).toBeTruthy();
        });
    });

    describe("Initialization", function(){
        var app;
        beforeEach(function(){
            app = new berlin.App();
        });

        it("contains route to home", function(){
            var name = "home";
            var uri = "/";
            app.initialize();
            expect(app.routes[name]).toBeDefined();
            expect(app.routes[name].match(uri)).toBeTruthy();
        });
    });

    xdescribe("Collections", function(){
        var app, initButtons, panels;
        var collectionNames = ['grupos', 'fichas', 'eventos'];
        beforeEach(function(){
            spyOn(berlin.Factory, 'getCollectionNames').and.returnValue(collectionNames);
            app = new berlin.App();
            app.initialize();
            initButtons = app.initButtons;
            panels = app.collectionPanels;
            spyOn(app.history, 'pushState');
        });

        it("adds state to history when initial button gets clicked", function(){
            util.runCallbackForEachArrayItem(collectionNames, function(name){
                initButtons[name].onClick();
                expect(app.history.pushState).toHaveBeenCalledWith('/' + name, name.toUpperCase());
            });
        });
        it("removes state from history when back button gets clicked", function(){
            var backButton = app.backButton;
            spyOn(app.history, 'popState');
            backButton.onClick();

            expect(app.history.popState).toHaveBeenCalled();
        });
        it("returns to previous state when back from collection uris", function(){
            initButtons.grupos.onClick();
            var backButton = app.backButton;
            spyOn(history, 'back');
            backButton.onClick();

            expect(history.back).toHaveBeenCalled();
        });
        it("get collections from the server", function(){
            var collectionName;
            spyOn(app.repo, 'getCollection').and.callFake(function(name, callback){
                expect(name).toBe(collectionName);
            });
            util.runCallbackForEachArrayItem(collectionNames, function(name){
                collectionName = name;
                initButtons[name].onClick();
            });
        });
    });

    describe("Crossroads", function(){
        var app;
        beforeEach(function(){
            app = new berlin.App();
            spyOn(app.history, 'pushState');
            spyOn(app.history, 'popState');
        });

        it("ignores state on initialization", function(){
            app.initialize();
            expect(crossroads.ignoreState).toBeTruthy();
        });
        it("not parses state on initialization if not necessary", function(){
            spyOn(crossroads, "parse").and.callThrough();
            spyOn(util, 'isNotFiringOnPopStateEventOnLoad').and.returnValue(false);
            app.initialize();
            expect(crossroads.parse).not.toHaveBeenCalled();
        });
        it("parses state on window.onpopstate", function(){
            spyOn(crossroads, "parse").and.callThrough();
            window.onpopstate({preventDefault: function(){
                expect(true).toBeTruthy();
            }});
            expect(crossroads.parse).toHaveBeenCalled();
        });
    });
})(SpecLib);
