/* jshint -W069 */
var SpecLib = SpecLib || {};

(function(){
    "use strict";
    function setSpyOnIfPossible(obj, method){
        if(obj[method].calls && obj[method].and) return obj[method];
        var spy =  spyOn(obj, method);
        return spy;
    }
    function createTestItems(){
        var testItem = {};

        testItem['grupos'] = {id:0, name:'testName',
                    fichas: [{id:0 , name: 'ficha0'}]};
        testItem['fichas'] = {id:0,
                    name:'testName',
                    nick:'testNick',
                    grupo: 'grupo0',
                    caracteristicas: [{id:0 , name: 'caracteristica0', value: 10}]};
        testItem['eventos'] = {id:0,
                    name:'testName',
                    date:'testDate',
                    grupo: 'grupo0',
                    lesion: '30',
                    caracteristicas: [{id:0 , name: 'caracteristica0', value: 10}]};
        testItem['caracteristicas'] = {id:0,
                    name:'testName',
                    alias:'T'};
        testItem['lesiones'] = {id:0,
                    name:'testName',
                    description:'testDescription'};

        return testItem;
    }
    function createTestCollections(){
        var testCollection = {};

        testCollection['grupos'] = [{id:0 , name: 'ficha0', alias: 'user0'},
                            {id:1 , name: 'ficha1', alias: 'user1'}];
        testCollection['fichas'] = [{id:0 , name: 'caracteristica0', alias: 'car0'},
                            {id:1 , name: 'caracteristica1', alias: 'car1'}];
        testCollection['eventos'] = [{id:0 , name: 'caracteristica0', alias: 'car0'},
                            {id:1 , name: 'caracteristica1', alias: 'car1'}];

        return testCollection;
    }
    function createTestTags(){
        var tags = {};

        tags['grupos'] = ['name', 'fichas'];
        tags['fichas'] = ['name', 'nick', 'grupo', 'caracteristicas'];
        tags['eventos'] = ['name', 'date', 'caracteristicas'];
        tags['caracteristicas'] = ['name', 'alias'];
        tags['lesiones'] = ['name', 'description'];

        return tags;
    }
    
    function createPanelWithId(id){
        return createPanel('#' + id);
    }
    function createPanelWithClass(className){
        return createPanel('.' + className);
    }
    function createPanel(selector){
        var attrib = (selector[0] === '.'?'class':'id');
        var selectorName = selector.substring(1);
        var self = $(selector);
        if (self.length === 0){
            console.log("the element with " + attrib + " '" + selectorName + "' has been created and appended to body");
            self = $("<div " + attrib + "='" + selectorName + "'></div>");
            $('body').append(self);
        }
        return self;
    }
    function createPromiseThatFailsStub(Args){
        return {
            done: function(){
                return {
                    error: function(callback){
                        callback(Args);
                    }
                };
            }
        };
    }
    function createPromiseThatPassStub(Args){
        return {
            done: function(callback){
                callback(Args);
                return {
                    error: function(){/* stub */}
                };
            }
        };
    }

    function expectElement (el) {
        var self = {},
            childElement;
        self.toHaveChild = function (index) {
            childElement = $(el.children()[index]);
            return self;
        };

        self.withId = function (id) {
            expect(childElement.attr("id"))
                .toEqual(id);
            return self;
        };
        self.withClass = function (cls) {
            expect(childElement.hasClass(cls)).
                toBeTruthy();
            return self;
        };

        return self;
    }



    SpecLib.setSpyOnIfPossible = setSpyOnIfPossible;
    SpecLib.createTestItems = createTestItems;
    SpecLib.createTestCollections = createTestCollections;
    SpecLib.createTestTags = createTestTags;

    SpecLib.createPanelWithId = createPanelWithId;
    SpecLib.createPanelWithClass = createPanelWithClass;
    SpecLib.createPromiseThatPassStub = createPromiseThatPassStub;
    SpecLib.createPromiseThatFailsStub = createPromiseThatFailsStub;
})(SpecLib);