gulp = require "gulp"
gutil = require "gulp-util"
webpack = require "webpack"
WebpackDevServer = require "webpack-dev-server"

gulp.task "webpack", (callback) ->
    # run webpack
    webpack {
        # configuration
    }, (err, stats) ->
        if err
            throw new gutil.PluginError "webpack", err
        gutil.log "[webpack]", stats.toString {
            # output options
        }
        callback()

gulp.task "webpack-dev-server", (callback) ->
    # Start a webpack-dev-server
    compiler = webpack {
        # configuration
    }

    new WebpackDevServer(compiler, {
        # server and middleware options
    }).listen 5001, "localhost", (err) ->
        if(err)
            throw new gutil.PluginError "webpack-dev-server", err
        # Server listening
        gutil.log "[webpack-dev-server]", "http://localhost:5001/webpack-dev-server/index.html"

        # keep the server alive or continue?
        # callback();

gulp.task "default", ["webpack-dev-server"], (callback) ->
