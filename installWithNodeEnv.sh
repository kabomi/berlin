#!/bin/sh
#$1: username or groupname used for chown $1:$1
bitbucketUser=kabomi
nodeVersion=0.12.7
npmVersion=2.11.3
rootDir=/var/www
optDir=/opt

#git config --global user.name "Imobach Martin"
#git config --global user.email "kabomi@gmail.com"

#fontconfig needed by phantomjs
sudo apt-get install fontconfig


sudo mkdir $rootDir
cd $rootDir
sudo git clone https://$bitbucketUser@bitbucket.org/kabomi/berlin.git
sudo chown -R $1:$1 berlin

#process.env.NODE_ENV
#env isolation with nodeenv
#in order to have multiple node versions in the same box and avoid conflicts
#if used, is better to install dependencies globally

cd $optDir
sudo apt-get install build-essential  g++
sudo curl https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py | python
sudo wget https://github.com/ekalinin/nodeenv/archive/0.7.1.tar.gz
sudo tar xf 0.7.1.tar.gz 
sudo chown -R $1:$1 nodeenv-0.7.1
python $optDir/nodeenv-0.7.1/nodeenv.py /var/www/berlin/env --node=$nodeVersion --npm=$npmVersion
cd $rootDir/berlin
. env/bin/activate
npm install grunt-cli@0.1.11 -g
npm install

unset bitbucketUser
unset nodeVersion
unset npmVersion
unset rootDir
unset optDir

export BERLIN_DEPLOY=true
grunt deploy
