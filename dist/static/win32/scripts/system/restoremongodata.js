var fs = require('fs');
var rootDir = '../../';
var mongodir = 'mongodata';
var mongobinDir = 'mongodb/bin/';

console.log(process.cwd());

process.chdir(rootDir);

if (fs.existsSync(mongodir)) {
        var Stats = fs.statSync('mongodata');
        if (Stats.isFile()){
		console.log('Se esperaba que mongodata fuera un directorio y no un archivo');
		process.exit(1);
	}
	if (Stats.isDirectory()){
		backup();
	}
}else{
	fs.mkdirSync(mongodir);
	backup();
}

function backup(){
		console.log('Start restore mongodata from backup');
		var exec = require('child_process').exec;
		exec('"' + mongobinDir + 'mongorestore.exe" --dbpath mongodata mongodata-backup/data-backup', function (error,stdout, stderr){
			console.log('stdout: ' + stdout);
			if (error !== null || stdout.indexOf('ERROR') >= 0){
				console.log(stderr);
				console.log('exec error: ' + error);
				process.exit(1);
			}else{
				console.log('Copia de seguridad restaurada con exito');
			}
		});
}